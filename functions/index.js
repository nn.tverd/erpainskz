const functions = require('firebase-functions');

const admin = require('firebase-admin');

admin.initializeApp(functions.config().firebase)
// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
exports.helloWorld = functions.https.onRequest((request, response) => {
    response.send("Hello Nikolay!");
});
const createNotification = ( notification =>{

    return admin.firestore().collection('notifications')
        .add(notification)
        .then((doc)=>{
            console.log( 'Norification added', doc )
        })
})

exports.projectCreated = functions.firestore
    .document('/projects/{projectId}')
    .onCreate(
        doc => {
            const project = doc.data()
            notification = {
                content: 'document is created',
                user: `${project.authorFirstName} ${project.authorLastName}`,
                timestamp: admin.firestore.FieldValue.serverTimestamp()
            }
            return createNotification( notification );
        }
    )

    exports.userCreated = functions.auth.user().onCreate(
        user => {
        return admin.firestore().collection('users').doc( user.uid ).get().then(
            doc =>{
                const user = doc.data()
                notification = {
                    content: 'Joined Party',
                    user: `${user.firstName} ${user.lastName}`,
                    timestamp: admin.firestore.FieldValue.serverTimestamp()
                }
                return createNotification( notification );
            }
        )
    })