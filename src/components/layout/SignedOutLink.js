import React from 'react'
import {NavLink} from 'react-router-dom';

const SignedOutLinks = () =>{
    return(
        <ul className="right">
            <li><NavLink to="/singup" className="">SingUp</NavLink></li>
            <li><NavLink to="/singin" className="">LogIn</NavLink></li>
        </ul>
                
    );
 
}

export default SignedOutLinks