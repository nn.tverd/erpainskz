import React from 'react'
import {Link} from 'react-router-dom';
import SignedInLinks from './SignedInLink';
import SignedOutLinks from './SignedOutLink';
import { connect } from 'react-redux'

const Navbar = (props) =>{
    const { auth } = props;
    // console.log( "const Navbar = (props) =>{", 'auth', auth )
    const links = auth.uid ? <SignedInLinks /> : <SignedOutLinks />;
    return(
        <nav className="nav-wrapper grey darken-3">
            <div className="container">
                <Link to="/" className="brand-logo">ERP</Link>
            </div>
            {links }
            
        </nav>
    );
 
}

const mapStateToProps = (state) => {
    // console.log( "const Navbar = () =>{", state )
    return {
        auth: state.firebase.auth
    }
}

export default connect(mapStateToProps)(Navbar)