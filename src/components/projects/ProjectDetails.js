import React from 'react'
import { compose } from 'redux';
import { connect } from 'react-redux';
import { firestoreConnect } from 'react-redux-firebase';
import { Redirect } from 'react-router-dom';
import moment from 'moment';

function ProjectDetails(props) {
    const { project, auth } = props;
    if (!auth.uid) return <Redirect to="/singup" />
    const id = props.match.params.id;
    // let date = null;
    // alert( project )
    // if( project.addedAt ) date = project.addedAt.toDate();
    if (project) {

        return (
            <div className="container section project-details">
                <div className="card z-depth-0">
                    <div className="card-contant">
                        <span className="card-title">{project.title} - {id}</span>
                        <p>{project.content}</p>
                    </div>
                    <div className="card-action grey lighten-4 grey-text">
                        <div>{moment(project.addedAt.toDate()).calendar()}</div>
                        <div>Posted by {project.authorFirstName} {project.authorLastName}</div>
                    </div>
                </div>

            </div>
        )
    } else {
        return (
            <div className="container section project-details">

                loading project....
            </div>
        )
    }
}
const mapStateToProps = (state, ownProps) => {
    const projects = state.firestore.data.projects;
    console.log("\n\n\n")
    console.log("V  V  V  V  V  V  V  V")
    console.log("\n\n\n")
    console.log(state.firestore.data)
    console.log("\n\n\n")
    console.log("A  A  A  A  A  A  A  A")
    console.log("\n\n\n")
    // console.log( "/n/n----/n", state, id );
    const id = ownProps.match.params.id;
    const project = projects ? projects[id] : null;
    return {
        project: project,
        auth: state.firebase.auth
    };
}
export default compose(
    connect(mapStateToProps),
    firestoreConnect([
        { collection: "projects" },
        {
            collection: "test",
            doc: "NdN5KHB3cyJJqBip2kEu",
            subcollections: [
                {
                    collection: "test3"
                }
            ], 
            storeAs: 'daily'
        }
    ])
)(ProjectDetails);

// export default ProjectDetails  NdN5KHB3cyJJqBip2kEu
