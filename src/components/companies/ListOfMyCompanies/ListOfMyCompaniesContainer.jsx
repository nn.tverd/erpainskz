import React from 'react'
import { connect } from 'react-redux'
import { addNewCompany, eraseFlagCompanyAdded } from '../../../../store/actions/companyActions';
import ListOfMyCompanies from './ListOfMyCompanies';

const mapStateToProps = (state) => {
    // console.log( "SOME STATE in ListOfMyCompanies", state )
    return{
        company_added_flag: state.company.recentlyAddedFlag,
        user_id: state.firebase.auth.uid
        
    }
}
const mapDispatchToProps = ( dispatch ) =>{
    return{
        addNewCompany: (company) => dispatch( addNewCompany(company) ),
        eraseFlagCompanyAdded: () => dispatch( eraseFlagCompanyAdded() )
    }
}

export default connect(mapStateToProps, mapDispatchToProps )(ListOfMyCompanies)