import React from 'react'
import { Redirect } from 'react-router-dom';

class ListOfMyCompanies extends React.Component {
    state = {
        company_name: '',
        company_description: "",
        company_logo_url: null,
        company_users:[],
        company_admins:[]
    }
    handleChange = (e)=> {
        this.setState({
            [e.target.id]: e.target.value
        });
    }
    handleSubmit = (e) => {
        e.preventDefault();
        this.props.addNewCompany( this.state )
    }
    
    render(){
        // console.log( "render(){", this.props )
        const company_added_flag = this.props.company_added_flag;
        if( company_added_flag ) {
            this.props.eraseFlagCompanyAdded( );
            return <Redirect to={`/user/${this.props.user_id}`} />
        }
        return (
            <div className="container grey-text">
                 <form onSubmit={this.handleSubmit} className="white">
                        <h5 className="grey-text text-darken-3">Add a new Company</h5>
                        <div className="input-field" >
                            <label htmlFor="company_name">Company Name</label>
                            <input type="text" id="company_name" onChange={this.handleChange} />
                        </div>
                        <div className="input-field" >
                            <label htmlFor="company_description">Company Description</label>
                            <input type="text" id="company_description" onChange={this.handleChange} />
                        </div>
                        <div className="input-field" >
                            <button className="btn pink lighten-1 z-depth-0">Add</button>
                        </div>
                    </form>
            </div>
        )
    } 
}
// export de
export default ListOfMyCompanies