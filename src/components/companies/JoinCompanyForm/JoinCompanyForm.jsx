import React from 'react'
import { connect } from 'react-redux'



class JoinCompanyForm extends React.Component {
    state = {
        company_id: ''
    }
    handleChange = (e)=> {
        this.setState({
            [e.target.id]: e.target.value
        });
    }
    handleSubmit = (e) => {
        e.preventDefault();
        // console.log( "class JoinCompanyForm extends Component", this.state )
        // console.log( "class JoinCompanyForm extends Component", this.props.st )
        // this.props.signIn( this.state )
    }
    
    render(){

        return (
            <div className="container grey-text">
                 <form onSubmit={this.handleSubmit} className="white">
                        <h5 className="grey-text text-darken-3">Join to a Company By ID</h5>
                        <div className="input-field" >
                            <label htmlFor="company_id">Company ID</label>
                            <input type="text" id="company_id" onChange={this.handleChange} />
                        </div>
                        <div className="input-field" >
                            <button className="btn pink lighten-1 z-depth-0">Join</button>
                        </div>
                    </form>
            </div>
        )
    } 
}
// export default JoinCompanyForm
const mapStateToProps = (state) => {
    return{
        company_id: state.company_id,
        st: state
    }
}
// const mapDispatchToProps = ( dispatch ) =>{
//     // console.log()
//     return{
//         signIn: (creds) => dispatch( signIn(creds) )
//     }
// }

export default connect(mapStateToProps, null )(JoinCompanyForm)