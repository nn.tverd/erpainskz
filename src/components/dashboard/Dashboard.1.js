import React, { Component } from 'react';
import Notifications from './Notifications';
import ProjectList from '../projects/ProjectsList';
import { connect } from 'react-redux'
// import { firestoreConnect } from 'react-redux-firebase'; //http://docs.react-redux-firebase.com/history/v3.0.0/docs/v3-migration-guide.html
import { compose } from 'redux';


class Dashboard extends Component {

    render() {
        console.log(this.props)
        const { projects } = (this.props);
        return (
            <div className="dashboard container">
                <div className="row">
                    <div className="col s12 m6">
                        <ProjectList projects={projects} key={projects.id} />
                    </div>
                    <div className="col s12 m5 m1-offset">
                        <Notifications />
                    </div>
                </div>
            </div>
        );
    }
}
const mapStateToProps = (state) => {
    console.log(state)
    return ({ projects: state.project.projects });
}
export default connect(mapStateToProps)(Dashboard);