import React, { Component } from 'react';
import Notifications from './Notifications';
import ProjectList from '../projects/ProjectsList';
// import { connect } from 'react-redux'
// import { firestoreConnect } from 'react-redux-firebase'; //http://docs.react-redux-firebase.com/history/v3.0.0/docs/v3-migration-guide.html
// import { compose } from 'redux';

const projects = {
    id: 4,
    list:[
        { id: 1, title: 2, content: 3 },
        
    ]
}

class Dashboard extends Component {

    render() {
        // console.log(this.props)
        // const { projects } = (this.props);
        return (
            <div className="dashboard container">
                <div className="row">
                    <div className="col s12 m6">
                        <ProjectList projects={projects.list} key={projects.id} />
                    </div>
                    <div className="col s12 m5 m1-offset">
                        <Notifications />
                    </div>
                </div>
            </div>
        );
    }
}
const mapStateToProps = (state) => {
    console.log(state)
    return ({ projects: state.project.projects });
}
// export default compose(
//     connect(mapStateToProps),
//     firestoreConnect([
//         { collection: 'projects' }
//     ])
// )(Dashboard);
export default Dashboard;