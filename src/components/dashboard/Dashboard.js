import React, { Component } from 'react';
import Notifications from './Notifications';
import ProjectList from '../projects/ProjectsList';
import { connect } from 'react-redux'
import { firestoreConnect } from 'react-redux-firebase'; //http://docs.react-redux-firebase.com/history/v3.0.0/docs/v3-migration-guide.html
import { compose } from 'redux';
import { Redirect } from 'react-router-dom';


class Dashboard extends Component {

    render() {
        console.log("class Dashboard extends Component ", this.props)
        const { projects, auth, notifications } = (this.props);
        if( !auth.uid ) return <Redirect to="/singup" />
        return (
            <div className="dashboard container">
                <div className="row">
                    <div className="col s12 m6">
                        <ProjectList projects={projects} />
                    </div>
                    <div className="col s12 m5 m1-offset">
                        <Notifications notifications={notifications} />
                    </div>
                </div>
            </div>
        );
    }
}
const mapStateToProps = (state) => {
    console.log('mapStateToProps DASHBOARD', state)
    return ({ 
        projects: state.firestore.ordered.projects,
        auth: state.firebase.auth,
        notifications: state.firestore.ordered.notifications
    });
}
// export default connect(mapStateToProps)(Dashboard);
export default  compose(
    connect( mapStateToProps ),
    firestoreConnect([
        {collection: 'projects', orderBy: ['addedAt', 'desc'] },
        {collection: 'notifications', limit: 3, orderBy: ['timestamp', 'desc'] }
    ])
)(Dashboard)