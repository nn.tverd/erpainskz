import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';

export const rrfConfig = {
    userProfile: 'users',
    // useFirestoreForProfile: true // Firestore for Profile instead of Realtime DB
}

export const fbConfig = {
    apiKey: "AIzaSyDEKIh-uFtidQSKmlpgEP-dCDCSYS_DM3M",
    authDomain: "ainserp.firebaseapp.com",
    databaseURL: "https://ainserp.firebaseio.com",
    projectId: "ainserp",
    storageBucket: "ainserp.appspot.com",
    messagingSenderId: "145843365046",
    appId: "1:145843365046:web:ec891468156517cc"
};

// Initialize Firebase
firebase.initializeApp(fbConfig);
firebase.firestore().settings({})

export default firebase;