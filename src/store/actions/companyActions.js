export const addNewCompany = (company) => {
    return (dispatch, getState, { getFirebase, getFirestore }) => {
        const state = getState();
        const curentUserId = state.curentUserId;
        const firestore = getFirestore();
        const firebase = getFirebase();
        console.log( "state.firebase.profile.", state.firebase.profile )
        const _users = [state.firebase.profile.id];
        firestore.collection('companies').add({
            ...company,
            creator: {
                creatorFirstName: state.firebase.profile.firstName,
                creatorLastName: state.firebase.profile.lastName,
                creatorId: state.firebase.profile.id
            },
            company_users: _users,
            addedAt: new Date(),
            // id: 12345
        }).then(() => {
            dispatch({
                type: "COMPANY_ADDED",
                project: company
            })
        }).catch((err) => {
            dispatch({
                type: "COMPANY_ADDED_FAILURE",
                err: err
            })
        })
    }
}
export const eraseFlagCompanyAdded = (  ) => {
    return (dispatch, getState, { getFirebase, getFirestore }) => {
        dispatch({
            type: "ERASE_FLAG_COMPANY_ADDED",
            // err: err
        })
    }
}

