export const createProject = (project) =>{
    return(
        (dispatch, getState, { getFirebase, getFirestore }) =>{
            //  async code to call database
            const firestore = getFirestore();
            const state = getState();
            firestore.collection('projects').add({
                ...project,
                authorFirstName: state.firebase.profile.firstName,
                authorLastName: state.firebase.profile.lastName,
                addedAt: new Date(),
                // id: 12345
            }).then(() =>{
                dispatch({
                    type: "CREATE_PROJECT",
                    project: project
                })
            }).catch((err) => {
                dispatch({
                    type: "CREATE_PROJECT_ERROR",
                    err: err
                })
            })
            
        }
    )
}