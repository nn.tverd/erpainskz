export const signIn = (credentials) => {
    return ( dispatch, getState, {getFirebase} ) => {
        const firebase = getFirebase();

        firebase.auth().signInWithEmailAndPassword(
            credentials.email,
            credentials.password
        ).then(() => {
            dispatch({type: 'LOGIN_SUCCESS'})
        }).catch((err)=>{
            dispatch({type: 'LOGIN_ERROR', err})
        })
    }
}

export const signOut = () =>{
    console.log( "export const signOut = () =>{ is called \n\n" ) 
    return( dispatch, getState, { getFirebase } ) => {
        const firebase = getFirebase();

        firebase.auth().signOut().then( () => {
            
            dispatch( { type: "SIGN_OUT_SUCCESS" } );
        } )
    }
}

export const  signUp = (newUser) =>{
    console.log( newUser )
    return (dispatch, getState, {getFirestore, getFirebase}) => {
        const firebase = getFirebase();
        const firestore = getFirestore();

        firebase.auth().createUserWithEmailAndPassword(
            newUser.email,
            newUser.password
        ).then((resp) =>{
            return firestore.collection("users").doc( resp.user.uid ).set({
                firstName: newUser.firstName,
                lastName: newUser.lastName,
                initials: newUser.firstName[0] + newUser.lastName[0],
                id: resp.user.uid
            })
        }).then(()=>{
            dispatch( {type: "SIGNUP_SUCCESS"} )
        }).catch(err =>{
            dispatch( {type: "SIGNUP_ERROR", err } )
        })
    }
}