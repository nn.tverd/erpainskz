const initState = {
    authError: null
}

const authReducer = (state = initState, action) => {
    switch(action.type){
        case 'LOGIN_SUCCESS':{
            console.log('login success')
            return {
                ...state,
                authError: null
            }
        }
        case 'LOGIN_ERROR':{
            console.log('login error')
            return {
                ...state,
                authError: 'login error'
            }
        }
        case 'SIGN_OUT_SUCCESS':{

            console.log( "\n\n\n authReducer Logged Out" );

            return state;
        }
        case 'SIGNUP_SUCCESS':{

            console.log( "\n\n\n SIGNUP_SUCCESS" );

            return {
                ...state,
                authError: null
            };
        }
        case 'SIGNUP_ERROR':{

            console.log( "\n\n\n SIGNUP_ERROR" );
            console.log( action.err.message )
            return {
                ...state,
                authError: action.err.message
            };
        }
        default:{
            return state;

        }
    }
}

export default authReducer;