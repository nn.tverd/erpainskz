const initState = {
    recentlyAddedFlag: false
}

const companyReducer = (state = initState, action) => {
    switch(action.type){
        case 'COMPANY_ADDED':{
            console.log('Company Added Reducer', action.company )

            return {
                ...state,
                recentlyAddedFlag: true
            };
            // return {
            //     ...state,
            //     authError: null
            // }
        }
        case 'COMPANY_ADDED_FAILURE':{
            console.log('Company Added Reducer Failure', action.err )
            return state;
        }
        case 'ERASE_FLAG_COMPANY_ADDED':{
            console.log('Company Added Reducer Failure', action.err )
            return {
                ...state,
                recentlyAddedFlag: false
            }
        }
        default:{
            return state;

        }
    }
}
export default companyReducer;