import authReducer from './authReducer';
import projectReducer from './projectReducer';
import companyReducer from './companyReducer';
import { combineReducers } from 'redux';
import { firebaseReducer } from 'react-redux-firebase'; // http://docs.react-redux-firebase.com/history/v3.0.0/docs/v3-migration-guide.html
import { firestoreReducer } from 'redux-firestore' // <- needed if using firestore

const rootReducer = combineReducers( {
    auth: authReducer,
    project: projectReducer,
    company: companyReducer,
    firebase: firebaseReducer,
    firestore: firestoreReducer // <- needed if using firestore

})


export default rootReducer;