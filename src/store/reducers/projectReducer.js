const initState = {
    projects:[
        {id:1, title:"eat mashrooms", content:"nam-nam-nam"},
        {id:2, title:"jump up", content:"uh-uh-uh"},
        {id:3, title:"just sleep", content:"oouuhh"},
    ]
}

const projectReducer = (state = initState, action) => {
    // console.log( "We are in the project Reducer" )
    switch( action.type ){
        case ("CREATE_PROJECT"):{
            console.log( "hello from reducer", "CREATE_PROJECT", action.project );
            return state;
        }
        case ("CREATE_PROJECT_ERROR"):{
            console.log( "hello from reducer", "CREATE_PROJECT_ERROR", action.err );
            return state;
        }
    }
    return state;
}

export default projectReducer;