import { createStore, combineReducers, compose } from 'redux'
import reducer from './store/reducers/rootReducer'
// import fbConfig, { rrfConfig } from './config/fbConfig'


const initialState = {}

export default () => {
  return createStore(
    reducer,
    initialState,
    // applyMiddleware(...middleware) // to add other middleware
  )
}