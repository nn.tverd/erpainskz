import React from 'react';
import './App.css';
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import Navbar from './components/layout/Navbar';
import Dashboard from './components/dashboard/Dashboard';
import ProjectDetails from './components/projects/ProjectDetails';
import SingUp from './components/auth/SingUp'
import SingIn from './components/auth/SignIn';
import CreateProject from './components/projects/CreateProject';
import PersonalCabinet from './components/auth/PersonalCabinet/PersonalCabinet';
import JoinCompanyForm from './components/companies/JoinCompanyForm/JoinCompanyForm';
import AddNewCompanyFormContainer from './components/companies/AddNewCompanyForm/AddNewCompanyFormContainer';

function App() {
    return (
        <BrowserRouter>
            <div className="App">
                <Navbar />
                <Switch>
                    <Route exact path="/" component={Dashboard} />
                    <Route path="/project/:id" component={ProjectDetails} />
                    <Route path="/singin" component={SingIn} />
                    <Route path="/singup" component={SingUp} />
                    <Route path="/create" component={CreateProject} />
                    <Route path="/user/:id" component={PersonalCabinet} />
                    <Route path="/addcompany" component={AddNewCompanyFormContainer} />
                    <Route path="/joincompany" component={JoinCompanyForm} />

                </Switch>
            </div>
        </BrowserRouter>
    );
}

export default App;
